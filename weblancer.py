#!/usr/bin/env python3.4
# -*- coding:utf-8 -*-
import urllib.request
from bs4 import BeautifulSoup
import re
import csv

BASE_URL = 'https://www.weblancer.net/jobs/'

def get_page_count(url):
    html = get_html(url)
    soup = BeautifulSoup(html, "html.parser")
    pagination = soup.find('ul', class_="pagination")
    pages = pagination.find_all('li')
    last_page = pages[-1]
    return int(re.search(r'(\d+)', str(last_page)).group())


def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()


def parse_projects(url):
    html = get_html(url)
    soup = BeautifulSoup(html, "html.parser")
    projects = []
    table = soup.find('div', class_='container-fluid cols_table show_visited')
    for row in table.find_all('div', class_='row'):
        cols = row.find_all('div')
        projects.append({
            "Tittle": (cols[0].h2.a.text).strip()
            , "Category": [category.text for category in cols[-1].find_all('a')]
            , "Price": (cols[1].text if len(cols[1].text) else "Договорная").strip()
            , "Application": (cols[2].text).strip()
        })
    return projects

def save_csv(projects, path):
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(("Проект", "Категория", "Стоимость", "Количество заявок"))

        for project in projects:
            writer.writerow(( project['Tittle']
                                , ', '.join(project['Category'])
                                , project['Price']
                                , project['Application']
                            ))
        print('\nprojects write success!\n')


def main():
    page_count = get_page_count(BASE_URL)
    projects = []
    for page in range(1, page_count+1):
        print("\n\nParsing\n\n %d%%" % int(page/page_count * 100))
        addr = BASE_URL + '?page=%d' % (page)
        projects.extend(parse_projects(addr))

    save_csv(projects, "projects.csv")



if __name__ == '__main__':
    main()